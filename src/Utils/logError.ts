import fs from 'fs-extra';
import os from 'os';

export default async function logError(command: string, error: Error | string) {
  if (!fs.existsSync(`${os.userInfo().homedir}/.securesign`)) {
    await fs.mkdir(`${os.userInfo().homedir}/.securesign`);
  }
  const errorString = `--------------------\n${new Date().toLocaleString('en-us')} | ${command} | ${error}\n--------------------\n`;
  await fs.appendFile(`${os.userInfo().homedir}/.securesign/cli.err`, errorString, {encoding: 'utf8'}).catch((e) => {
    throw e;
  });
  return await true;
}
