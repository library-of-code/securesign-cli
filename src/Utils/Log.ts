import chalk from 'chalk';

export default class Log {
  constructor() {}

  /**
   * Logs an error to the console, this is red.
   * @param type Type 0 is bold, type 1 is not.
   * @param message The message to log.
   * @example Log.error(0, 'An error has occurred, please try again later.');
   */
  public error(type: number = 0, message: string): void {
    if (type === 0) {
      console.error(chalk.bold.red(message));
    } else if (type === 1) {
      console.error(chalk.red(message));
    } else {
      throw new RangeError('Type is invalid.');
    }
  }

  /**
   * Logs an informational message to the console, this is a dim blue.
   * @param message The message to log.
   * @example Log.info('Writing file...');
   */
  public info(message: string): void {
    console.log(chalk.blue(message));
  }

  /**
   * Logs a success message to the console, this is green.
   * @param type Type 0 is bold, type 1 is not.
   * @param message 
   * @example Log.success(0,'The operation completed without error.');
   */
  public success(type: number = 0, message: string): void {
    if (type === 0) {
      console.log(chalk.bold.green(message));
    } else if (type === 1) {
      console.log(chalk.green(message));
    } else {
      throw new RangeError('Type is invalid.');
    }
  }

  /**
   * Logs a field to the console.
   * @param title The title of the message, this is shown in bold.
   * @param message The "body" of the message.
   * @param eol Specifies if you want an EOL (end of line) statement at the end.
   * @example Log.field('ID', '7878541002ab541', true);
   */
  public field(title: string, message: string, eol: boolean = true): void {
    console.log(`${chalk.bold(`${title}:`)} ${message} ${eol ? '\n' : ''}`);
  }
}