import logError from '../Utils/logError';
import axios, { AxiosResponse } from 'axios';
import fs from 'fs-extra';
import os from 'os';
import Log from '../Utils/Log';

export default async function listCerts(): Promise<void> {
  const st: Log = new Log();
  try {
    let auth: string;
    if (fs.existsSync(`${os.userInfo().homedir}/.securesign/auth`)) {
      const f: string = await fs.readFile(`${os.userInfo().homedir}/.securesign/auth`, {encoding: 'utf8'});
      auth = f.replace(/\r?\n|\r/g, '');
    } else {
      return st.error(0, 'Please run the init subcommand before checking issued certificates.');
    }
    const method: AxiosResponse<any> = await axios({
      method: 'get',
      url: 'https://api.securesign.org/certificates',
      headers: {'Authorization': auth}
    }); 
    method.data.message.forEach((x: { Certificate: { subject: { commonName: string; emailAddress: string; }; serial: string; notAfter: Date; extensions: { extendedKeyUsage: string; }; issuer: { organizationName: string; commonName: string; }; }; URL: string; }) => {
      try {
        st.field('Subject Common Name', x.Certificate.subject.commonName ? x.Certificate.subject.commonName : 'Not Specified');
        st.field('Subject Email Address', x.Certificate.subject.emailAddress ? x.Certificate.subject.emailAddress : 'Not Specified');
        st.field('Serial Number', x.Certificate.serial ? x.Certificate.serial : 'Not Specified');
        st.field('Expires On', x.Certificate.notAfter ? new Date(x.Certificate.notAfter).toLocaleString('en-us') : 'Not Specified');
        st.field('Extended Key Usages', x.Certificate.extensions.extendedKeyUsage ? x.Certificate.extensions.extendedKeyUsage : 'Not Specified');
        st.field('Issuer Organization', x.Certificate.issuer.organizationName ? x.Certificate.issuer.organizationName : 'Not Specified');
        st.field('Issuer Common Name', x.Certificate.issuer.commonName ? x.Certificate.issuer.commonName : 'Not Specified');
        st.field('Download Link', x.URL ? x.URL : 'Not Specified');
        console.log('\n'); 
      } catch {}
    });
    st.success(0, 'Your list of generated certificates are above.');
  } catch (err) {
    await logError('listcerts', err);
    return st.error(0, 'An error has occurred during processing. Please check the error logs for more information');
  }
}