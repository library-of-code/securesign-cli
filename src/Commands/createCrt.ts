import logError from '../Utils/logError.js';
import chalk from 'chalk';
import os from 'os';
import fs from 'fs-extra';
import axios, { AxiosResponse } from 'axios';
import inquirer from 'inquirer';
//import ProgressBar from 'progress';

export default async function createCrt(cmd: { interactive?: boolean, auth?: string, sign?: string, class?: number, digest?: string, in?: string, out?: string }): Promise<void> {
  if (cmd.interactive) {
    try {
      const init: any = await inquirer.prompt([
        {
          type: 'list',
          message: 'Do you want to make an ECC or RSA certificate?',
          name: 'signType',
          choices: ['ECC', 'RSA']
        }, 
        {
          type: 'list',
          message: 'What digest algorithm would you like to use?',
          name: 'md',
          choices: ['SHA256', 'SHA384', 'SHA512']
        },
        {
          type: 'list',
          message: 'What class is this certificate going to be?',
          name: 'class',
          choices: ['1 [Domain Validation]', '2 [Organization Validation]', '3 [Extended Validation]']
        }
      ]);
      let auth: string;
      if (cmd.auth) {
        auth = cmd.auth;
      } else if (fs.existsSync(`${os.userInfo().homedir}/.securesign/auth`)) {
        const f: string = await fs.readFile(`${os.userInfo().homedir}/.securesign/auth`, {encoding: 'utf8'});
        auth = f.replace(/\r?\n|\r/g, '');
      } else if (process.env.SSAUTH) {
        auth = process.env.SSAUTH;
      } else {
        await logError('createcrt', 'Authorization must be supplied, received none.'); return console.error(chalk.bold.red('Authorization must be supplied.'));
      }
  
      const signType: string = init.signType ? init.signType : 'ecc';
      const md: string = init.md ? init.md.toLowerCase() : 'sha256';
      const outFile: string = cmd.out ? cmd.out : `${os.userInfo().homedir}/Validation/${os.userInfo().username}.crt`;
      const classID: number = init.class ? Number(init.class.split(' ')[0]) : 1;
      const inFile: string = cmd.in ? cmd.in : `${os.userInfo().homedir}/.securesign/default.csr`;
      const csrFile: string = await fs.readFile(inFile, {encoding: 'utf8'});
      if (!csrFile) {
        await logError('createcrt', 'Cannot locate CSR'); return console.error(chalk.bold.red('Cannot locate CSR.'));
      }
      console.log(chalk.dim('This may take a moment, do not kill this process at any time.'));
      console.log(chalk.blue('Generating certificate...'));
      const method: AxiosResponse<any> = await axios({
        method: 'post',
        url: `https://api.securesign.org/certificates/${signType}/client/?class=${classID}`,
        headers: {'Authorization': auth, 'Content-Type': 'application/json'},
        data: JSON.stringify({commonname: `${os.userInfo().username}@cloud.libraryofcode.us`, md: md, csr: csrFile}),
      });
      console.log(chalk.magenta(`Received Certificate\nIssued by: ${method.data.message.Certificate.issuer.commonName}`));
      const certificate: AxiosResponse<any> = await axios({
        method: 'get',
        url: method.data.message.URL,
      });
      //progress.tick();
      if (!fs.existsSync(`${os.userInfo().homedir}/Validation/`)) {
        console.log(chalk.blue('Validation directory does not exist, creating...'));
        await fs.mkdir(`${os.userInfo().homedir}/Validation/`);
        console.log(chalk.italic.green('Created Validation directory.'));
      }
      console.log(chalk.blue('Writing certificate...'));
      await fs.writeFile(outFile, certificate.data, {encoding: 'utf8'});
      //progress.tick();
      console.log(chalk.bold.green('Successfully wrote certificate.'));
    } catch (err) {
      await logError('createcrt', err); return console.error(chalk.bold.red('An error has occurred during processing. Please check the error logs for more information.'));
    }
  }
  else {
    try {
      let auth: string;
      if (cmd.auth) {
        auth = cmd.auth;
      } else if (fs.existsSync(`${os.userInfo().homedir}/.securesign/auth`)) {
        const f: string = await fs.readFile(`${os.userInfo().homedir}/.securesign/auth`, {encoding: 'utf8'});
        auth = f.replace(/\r?\n|\r/g, '');
      } else if (process.env.SSAUTH) {
        auth = process.env.SSAUTH;
      } else {
        await logError('createcrt', 'Authorization must be supplied, received none.'); return console.error(chalk.bold.red('Authorization must be supplied.'));
      }
      /*const progress: ProgressBar = new ProgressBar('[:bar] :current/:total :percent | :eta\n', { 
        total: 3,
        complete: '#',
        incomplete: '-',
      });*/
      const signType: string = cmd.sign ? cmd.sign.toLowerCase() : 'ecc';
      const md: string = cmd.digest ? cmd.digest.toLowerCase() : 'sha256';
      const outFile: string = cmd.out ? cmd.out : `${os.userInfo().homedir}/Validation/${os.userInfo().username}.crt`;
      const classID: number = cmd.class ? Number(cmd.class) : 1;
      const inFile: string = cmd.in ? cmd.in : `${os.userInfo().homedir}/.securesign/default.csr`;
      const csrFile: string = await fs.readFile(inFile, {encoding: 'utf8'});
      if (!csrFile) {
        await logError('createcrt', 'Cannot locate CSR'); return console.error(chalk.bold.red('Cannot locate CSR.'));
      }
      //progress.tick();
      console.log(chalk.dim('This may take a moment, do not kill this process at any time.'));
      console.log(chalk.blue('Generating certificate...'));
      const method: AxiosResponse<any> = await axios({
        method: 'post',
        url: `https://api.securesign.org/certificates/${signType}/client/?class=${classID}`,
        headers: {'Authorization': auth, 'Content-Type': 'application/json'},
        data: JSON.stringify({commonname: `${os.userInfo().username}@cloud.libraryofcode.us`, md: md, csr: csrFile}),
      });
      console.log(chalk.magenta(`Received Certificate\nIssued by: ${method.data.message.Certificate.issuer.commonName}`));
      const certificate: AxiosResponse<any> = await axios({
        method: 'get',
        url: method.data.message.URL,
      });
      //progress.tick();
      if (!fs.existsSync(`${os.userInfo().homedir}/Validation/`)) {
        console.log(chalk.blue('Validation directory does not exist, creating...'));
        await fs.mkdir(`${os.userInfo().homedir}/Validation/`);
        console.log(chalk.italic.green('Created Validation directory.'));
      }
      console.log(chalk.blue('Writing certificate...'));
      await fs.writeFile(outFile, certificate.data, {encoding: 'utf8'});
      //progress.tick();
      console.log(chalk.bold.green('Successfully wrote certificate.'));
    } catch (err) {
      await logError('createcrt', err); return console.error(chalk.bold.red('An error has occurred during processing. Please check the error logs for more information.'));
    }
  }
}
