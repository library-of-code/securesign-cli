import logError from '../Utils/logError';
import chalk from 'chalk';
import os from 'os';
import fs from 'fs-extra';
import axios, { AxiosResponse } from 'axios';

export default async function activateKey(key: string, cmd: { auth?: string }): Promise<void> {
  try {
    let auth: string;
    if (cmd.auth) {
      auth = cmd.auth;
    } else if (fs.existsSync(`${os.userInfo().homedir}/.securesign/auth`)) {
      const f: any = await fs.readFile(`${os.userInfo().homedir}/.securesign/auth`, {encoding: 'utf8'});
      auth = f.replace(/\r?\n|\r/g, '');
    } else if (process.env.SSAUTH) {
      auth = process.env.SSAUTH;
    } else {
      await logError('activatekey', 'Authorization must be supplied, received none.'); return console.error(chalk.bold.red('Authorization must be supplied.'));
    }
    console.log(chalk.blue('Activating key...'));
    const method: AxiosResponse<any> = await axios({
      method: 'POST',
      url: 'https://api.securesign.org/account/keys/activation',
      headers: {'Authorization': auth, 'Content-Type': 'application/json'},
      data: JSON.stringify({key: key}),
    });
    if (method.status === 200) {
      console.log(chalk.bold.green('Activation Key Accepted.'));
    } else {
      await logError('activatekey', method.data);
      console.log(chalk.bold.red('An error has occurred during processing, please check the logs for more information.'));
    }
  } catch (err) {
    await logError('createcrt', err); return console.error(chalk.bold.red('An error has occurred during processing. Please check the error logs for more information.'));
  }
}
