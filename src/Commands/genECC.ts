import logError from '../Utils/logError.js';
import chalk from 'chalk';
import os from 'os';
import fs from 'fs-extra';
import axios, { AxiosResponse } from 'axios';

export default async function genECC(curve: string, cmd: any): Promise<any> {
  try {
    let auth: string;
    if (cmd.auth) {
      auth = cmd.auth;
    } else if (fs.existsSync(`${os.userInfo().homedir}/.securesign/auth`)) {
      const f: string = await fs.readFile(`${os.userInfo().homedir}/.securesign/auth`, {encoding: 'utf8'});
      auth = f.replace(/\r?\n|\r/g, '');
    } else if (process.env.SSAUTH) {
      auth = process.env.SSAUTH;
    } else {
      await logError('genecc', 'Authorization must be supplied, received none.'); return console.error(chalk.bold.red('Authorization must be supplied.'));
    }
    const method: AxiosResponse<any> = await axios({
      method: 'POST',
      url: 'https://api.securesign.org/keys/ecc',
      headers: {'Authorization': auth, 'Content-Type': 'application/json'},
      data: JSON.stringify({curve: curve}),
    });
    if (cmd.out) {
      console.log(chalk.blue('Writing private key...'));
      await fs.writeFile(cmd.out, method.data.message, {encoding: 'utf8'});
      return console.log(chalk.bold.green('Successfully wrote private key.'));
    }
    if (cmd.overwrite) {
      if (fs.existsSync(`${os.userInfo().homedir}/.securesign`)) {
        console.log('Overwriting private key...');
        await fs.writeFile(`${os.userInfo().homedir}/.securesign/default.key.pem`, method.data.message, {encoding: 'utf8'});
        return console.log(chalk.bold.green('Successfully overwrote private key.'));
      } else {
        return console.log(chalk.red('Please run "securesign init" first before running this command.'));
      }
    }
    console.log(method.data.message);
  } catch (err) {
    await logError('genecc', err);
    return console.error(chalk.bold.red('An error has occurred during processing. Please check the error logs for more information'));
  }
}
