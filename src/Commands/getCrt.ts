import os from 'os';
import fs from 'fs-extra';
import axios, { AxiosResponse } from 'axios';
import logError from '../Utils/logError';
import Log from '../Utils/Log';

export default async function getCrt(serial: string, cmd: any) {
  const st: Log = new Log();
  try {
    let auth: string;
    if (fs.existsSync(`${os.userInfo().homedir}/.securesign/auth`)) {
      const f: string = await fs.readFile(`${os.userInfo().homedir}/.securesign/auth`, {encoding: 'utf8'});
      auth = f.replace(/\r?\n|\r/g, '');
    } else {
      return st.error(0, 'Please run the init subcommand before checking issued certificates.');
    }
    const outFile: string = cmd.out ? cmd.out : `${os.userInfo().homedir}/Validation/${os.userInfo().username}.crt`;
    const method: AxiosResponse<any> = await axios({
      method: 'get',
      url: 'https://api.securesign.org/certificates',
      headers: {'Authorization': auth}
    });
    st.info('Received list, filtering...');
    const filter = method.data.message.filter((m: any) => m.Certificate.serial === serial);
    if (!filter.length) {
      return st.error(0, 'Could not locate a certificate with this serial number on your account.');
    }
    st.success(1, 'Found match for certificate serial number.');
    st.info('Requesting certificate.');
    const certRequest: AxiosResponse<any> = await axios({
      method: 'get',
      url: filter[0].URL
    });
    st.success(1, 'Certificate received.');
    if (cmd.write === true) {
      if (!fs.existsSync(`${os.userInfo().homedir}/Validation/`)) {
        st.info('Validation directory does not exist, creating...');
        await fs.mkdir(`${os.userInfo().homedir}/Validation/`);
        st.success(1, 'Created Validation directory.');
      }
      st.info('Writing certificate to Validation directory...');
      await fs.writeFile(outFile, certRequest.data, { encoding: 'utf8' });
      st.success(0, 'Successfully wrote certificate.');
    } else {
      st.field('Subject Common Name', filter[0].Certificate.subject.commonName ? filter[0].Certificate.subject.commonName : 'Not Specified');
      st.field('Subject Email Address', filter[0].Certificate.subject.emailAddress ? filter[0].Certificate.subject.emailAddress : 'Not Specified');
      st.field('Serial Number', filter[0].Certificate.serial ? filter[0].Certificate.serial : 'Not Specified');
      st.field('Expires On', filter[0].Certificate.notAfter ? new Date(filter[0].Certificate.notAfter).toLocaleString('en-us') : 'Not Specified');
      st.field('Extended Key Usages', filter[0].Certificate.extensions.extendedKeyUsage ? filter[0].Certificate.extensions.extendedKeyUsage : 'Not Specified');
      st.field('Issuer Organization', filter[0].Certificate.issuer.organizationName ? filter[0].Certificate.issuer.organizationName : 'Not Specified');
      st.field('Issuer Common Name', filter[0].Certificate.issuer.commonName ? filter[0].Certificate.issuer.commonName : 'Not Specified');
      st.field('Download Link', filter[0].URL ? filter[0].URL : 'Not Specified');
      st.info('If you didn\'t want to view the information and wanted to overwrite your current certificate. Pass the "-w" option to the command.');
    }
  } catch (err) {
    await logError('getcrt', err);
    return st.error(0, 'An error has occurred during processing. Please check the error logs for more information');
  }
}