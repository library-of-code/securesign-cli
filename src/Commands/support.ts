import logError from '../Utils/logError.js';
import os from 'os';
import fs from 'fs-extra';
import chalk from 'chalk';
import inquirer from 'inquirer';
import Log from '../Utils/Log';
import mailer from 'nodemailer';

export default async function support() {
  const st: Log = new Log();
  try {
    const f: string = await fs.readFile(`${os.userInfo().homedir}/.securesign/auth`, {encoding: 'utf8'});
    const auth = f.replace(/\r?\n|\r/g, '');
  
    console.log(chalk.bold('Library of Code sp-us | Cloud Services'));
    st.info('You are about to send a support request, if you didn\'t mean to do this, please exit now.');
  
    const quest: {password: string, department: string, subject: string, replyToConfirm: boolean} = await inquirer.prompt([
      {
        type: 'password',
        message: 'Please enter your Cloud Services password',
        name: 'password',
        mask: '*'
      },
      {
        type: 'list',
        message: 'Which department would you like to contact?',
        name: 'department',
        choices: ['Support Team | support@libraryofcode.us', 'Moderation Team | moderation@libraryofcode.us', 'Community Management | management@libraryofcode.us', 'Engineering Team | engineering@libraryofcode.us']
      },
      {
        type: 'input',
        message: 'What is the subject of this support case?',
        name: 'subject'
      },
      /*{
        type: 'confirm',
        message: `By default, replies to the ticket will be sent to <${os.userInfo().username}>, do you want them to be sent somewhere else?`,
        name: 'replyToConfirm',
        default: false
      }*/
    ]);
    let fromAddress: string;
    quest.replyToConfirm = false;
    if (quest.replyToConfirm) {
      const replyAddress: {default: string} = await inquirer.prompt([{
        type: 'input',
        message: 'Please enter the email address you want to receive replies to.',
        name: 'default'
      }]);
      fromAddress = replyAddress.default;
    } else fromAddress = `${os.userInfo().username}@cloud.libraryofcode.us`;
  
    const body: {default: string} = await inquirer.prompt([{
      type: 'editor',
      message: 'Please enter the message you want to send to our team, include as much information as possible. When you are done save the document as usual.\n',
      name: 'default'
    }]);
  
    const transport = mailer.createTransport({
      host: 'cloud.libraryofcode.us',
      port: 25,
      secure: false,
      auth: {user: os.userInfo().username, pass: quest.password}
    });
  
    await transport.sendMail({
      from: fromAddress,
      to: quest.department.split('| '),
      subject: quest.subject,
      text: body.default
    });
  
    st.success(0, `Your ticket has been successfully submitted to ${quest.department}, please check your email address for further updates.`);
  } catch (err) {
    await logError('support', err);
    return st.error(0, 'An error has occurred during processing, please check the logs for further information.');
  }
}