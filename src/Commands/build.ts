import chalk from 'chalk';
import logError from '../Utils/logError';
import Log from '../Utils/Log';
const fs = require('fs-extra');

interface Info {
  checkSums: {
    md5: string,
    sha256: string,
    sha512: string
  }, 
  build: {
    date: Date,
    system: string
  },
  package: {
    version: string,
    channel: string
  }
}

export default async function build(): Promise<void> {
  try {
    let infoJson: string;
    if (process.execPath.includes('canary')) {
      infoJson = await fs.readFile('/opt/securesign/canary/data/info.json', { encoding: 'utf8' });
    } 
    else if (process.execPath.includes('beta')) {
      infoJson = await fs.readFile('/opt/securesign/beta/data/info.json', { encoding: 'utf8' });
    } 
    else if (process.execPath.includes('stable')) {
      infoJson = await fs.readFile('/opt/securesign/stable/data/info.json', { encoding: 'utf8' });
    }
    else {
      throw Error('Cannot locate process execute path.');
    }
    const info: Info = JSON.parse(infoJson);
    console.log(chalk.bold.underline('SecureSign CLI Build Information'));
    //console.log(chalk.dim('Build Information'));
    console.log(`${chalk.bold('Maintainer: ')} Library of Code sp-us | Engineering Team <engineering@libraryofcode.us>`);
    console.log(`${chalk.bold('Version: ')} ${info.package.version}`);
    console.log(`${chalk.bold('Language: ')} TypeScript`);
    console.log(`${chalk.bold('Channel: ')} ${info.package.channel}`);
    console.log(`${chalk.bold('License: ')} Proprietary`);
    console.log(`${chalk.bold('Built For: ')} ${info.build.system}`);
    console.log(`${chalk.bold('Built On: ')} ${info.build.date}`);
    console.log(`${chalk.bold('SHA256 Checksum: ')} ${info.checkSums.sha256}`);
  } catch (err) {
    await logError('build', err);
    const log = new Log();
    log.error(0, 'An error has occurred during processing, please check the error logs for more information.');
  }
}